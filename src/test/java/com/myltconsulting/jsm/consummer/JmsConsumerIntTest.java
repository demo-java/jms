package com.myltconsulting.jsm.consummer;

import com.myltconsulting.jsm.producer.JmsProducer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class JmsConsumerIntTest {

    @Autowired
    private JmsConsumer jmsConsumer;

    @Autowired
    private JmsProducer jmsProducer;

    @Test
    public void consumerHaveCatchTheMessageSend(){

        String message = "mylt consulting";
        this.jmsProducer.sendMessage(message);
        assertThat(this.jmsConsumer.getMessageReceived()).isEqualTo(message);

    }

}
