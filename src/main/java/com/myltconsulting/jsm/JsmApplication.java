package com.myltconsulting.jsm;

import com.myltconsulting.jsm.consummer.JmsConsumer;
import com.myltconsulting.jsm.producer.JmsProducer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.jms.JMSException;

@SpringBootApplication
public class JsmApplication {

	public static void main(String[] args) throws JMSException {
		SpringApplication.run(JsmApplication.class, args);

	}

}
