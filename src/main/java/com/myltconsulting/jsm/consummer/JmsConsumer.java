package com.myltconsulting.jsm.consummer;


import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;



@Component
public class JmsConsumer  {

   private String messageReceived;

    @JmsListener(destination = "${active-mq.queue}")
    public void onMessage(String message) throws Exception {
        this.setMessageReceived(message);
    }

    public String getMessageReceived() {
        return messageReceived;
    }

    public void setMessageReceived(String messageReceived) {
        this.messageReceived = messageReceived;
    }
}
