# SPRING JMS

## Description

This project aim is to test Spring jms with activeMq which run in standalone mode.


## Prerequisite

- JDK 8 or above




## Maven Dependencies for Active Mq

```xml

<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-activemq</artifactId>
</dependency>
```


## How to run

```bash
mvn spring-boot:run
```

## How to build

```bash
mvn package
```

## How install Active Mq

You can use emedded Active Mq comming from spring-boot-starter-activemq
and start the broker manually :

```
BrokerService broker = BrokerFactory.createBroker(new URI(
                "broker:(tcp://localhost:61616)"));
broker.start();
```

1) Download ActiveMQ binary from [here](https://activemq.apache.org/download.html)
2) Unzip your bundle, and Open Terminal.
3) Set the Terminal path to ActiveMQ -> bin
4) Write command activemq start  [windows]




